# Boats Group

## Interview Phase II

### Assignment

Write an application that builds a UI similar to this one using the data provided in data.json

[Sample Widget](https://bitbucket.org/boats-group/interview_project/raw/0b7583d006d0de5895afee80bf1e0cd0b43db422/img/test.gif)

### Rules:

1. You have 24 hours from the moment we hit send to the moment we receive your email with the project
2. You need to provide a public git repository link with the code for the completed project. preferably Github, Gitlab or Bitbucket
3. You can use whatever framework you feel comfortable with including, but not limited to:
   - No framework, Vanilla Javascript
   - React
   - Angular
   - Vue
   - Jquery
   - Bootstrap
4. You must include instruction on how to run your project in a README.md file at the root of your repo
5. The styles are not final and we encorage you to improve, although the interaction described below is required
   1. Tabbed View with tabs below and panel above
   2. Tabs must be clickable and should change the content of the panel without navigating away
   3. Icons must be the ones presented: font-awesome free version Link to the projectThe Icons are:
      - User
      - Email
      - Calendar
      - Phone
      - Map Marked

### Credits

#### Basic

Implement the interaction as depicted

#### Extra 1

Use an http request (fetch) to https://randomuser.me/api/?results=1&noinfo and get the data instead of using the data in the json file

#### Extra 2

Use modular JS and create/use a build process
